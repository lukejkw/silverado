This website was developed by Luke Warren for RMIT University

----------------------------------------------------------
Website Details
----------------------------------------------------------
Name: Silverado
Logo icon: <div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a>             is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></div>
----------------------------------------------------------
Styling
----------------------------------------------------------
Primary Colour (blue) #2b58e7
Secondary Colour (black) #000000
Tertiary Colour (white) #ffffff
