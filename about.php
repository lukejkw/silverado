<?php require("Components/Common.php") ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Common meta -->
        <?php require('Fragments/meta.php') ?>
        <!-- Page specific meta -->
        <meta name="description" content="desc">
        <meta name="keywords" content="silverado,movies,new">
        <link rel="shortcut icon" href="favicon.ico">
        <title>Silverado movie house</title>
        
        <!-- Client Resources -->
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="Styles/semantic.min.css"><!-- UI library http://semantic-ui.com/ -->
        <link type="text/css" rel="stylesheet" href="Styles/global_styles.css">
        <!-- Scripts -->
        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script><!-- JQuery JS library -->
        <script type="text/javascript" src="Scripts/semantic.min.js"></script><!-- UI library http://semantic-ui.com/ -->
        <script type="text/javascript" src="Scripts/about.js"></script>
    </head>
    <body>
        <section>
            <!-- Header -->
            <?php require('Fragments/header.php') ?>
            
            <!-- Content -->
            <h2 class="ui top attached inverted header">
                <i class="help icon"></i>
                <div class="content">
                    About us
                    <div class="sub header">Who, where and what?</div>
                </div>
            </h2>
            <div id="main-body" class="ui attached inverted segment">
                <div class="ui fluid stackable grid">
                    <div class="eight wide column">
                        <h2>
                            Who are we?
                        </h2>
                        <p>
                            Silverado is a a small country cinema with big entertainment!
                        </p>
                        <p>
                            In our state of the art, newly renovated theatres, we show the latest in movie entertainment. Come and check us out today!
                        </p>
                        <h2>
                            Contact Details
                        </h2>
                        <p>
                            <label for="tel"><strong>Call: </strong></label> 
                            <span name="tel">
                                123 Road, Silverado, QLD, Australia
                            </span>
                            <strong>Tel: </strong> 011-123-4567
                        </p>
                        <a href="#" class="ui basic white inverted icon button">
                            <i class="facebook icon"></i>
                        </a>
                        <a href="#" class="ui basic white inverted icon button">
                            <i class="twitter icon"></i>
                        </a>
                        <!-- <a> Launches contact modal -->
                        <a id="contactUs" href="#" class="ui basic white inverted button contactClick ui icon item">
                            Contact Us
                        </a>
                    </div>
                    <div class="eight wide column">
                        <h2>
                            Where to find us
                        </h2>
                        <p>
                            <label for="addr"><strong>Address: </strong></label> 
                            <span name="addr">
                                123 Road, Silverado, QLD, Australia
                            </span>
                        </p>
                        <p>
                            <?php require('Fragments/map.html')?>
                        </p>
                    </div>
                </div>
            </div>
            
            <!-- Footer -->
            <?php require('Fragments/footer.html') ?>
        </section>
    </body>
</html>