<?php 

require_once("Models/Booking.php"); 

require_once("Components/ProcessCart.php"); 

require_once("Components/Common.php"); 

// POST code listener for checkout form
require_once("Components/ProcessCheckout.php");

ProcessCheckout()

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Common meta -->
        <?php require('Fragments/meta.php') ?>
        <!-- Page specific meta -->
        <meta name="description" content="desc">
        <meta name="keywords" content="silverado,movies,new">
        <link rel="shortcut icon" href="favicon.ico">
        <title>Silverado movie house</title>
        
        <!-- Client Resources -->
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="Styles/semantic.min.css"><!-- UI library http://semantic-ui.com/ -->
        <link type="text/css" rel="stylesheet" href="Styles/global_styles.css">
        <link type="text/css" rel="stylesheet" href="Styles/movies.css">
        <!-- Scripts -->
        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script><!-- JQuery JS library -->
        <script type="text/javascript" src="Scripts/semantic.min.js"></script><!-- UI library http://semantic-ui.com/ -->
    </head>
    <body>
        <section>            
           
            <?php 
            
            // Header
            require('Fragments/header.php');
            
            ?>

            <!-- Content -->
            <h2 class="ui top attached inverted header">
                <i class="film icon"></i>
                <div class="content">
                    Checkout
                    <div class="sub header">Finalize your bookings</div>
                </div>
            </h2>
            
            <div id="main-body" class="ui attached inverted segment">
                
                <div class="ui inverted segment">
                    <h3>
                        Booking Order Details
                    </h3>
                    <p>
                        <strong>Total: </strong> $ <?php echo (CalculateTotal()) ?>.00
                    </p>
                    
                    <p <?php echo (DiscountAvailable() ? "" : "style='display:none;'") ?>>
                        <strong>Discount (20 %): </strong> $ <?php echo (CalculateDiscount()) ?>.00
                    </p>
                    
                    <p>
                        You can go back to your <a href="cart.php">Cart</a> at any time to review your order.
                    </p>
                </div>
                
                <form class="ui inverted form segment" 
                    method="POST" 
                    action="checkout.php">
                    <h3>
                        Please fill in your details to finalize your booking
                    </h3>
                    <div class="field">
                        <label for="first"></label>
                        <input type="text" placeholder="First Name" pattern="^[^±!@£$%^&*_+§¡€#¢§¶•ªº«\\/<>?:;|=.,]{1,20}$" title="First name is required and connot contain illegal characters" name="first" required>
                    </div>
                    <div class="field">
                        <label for="last"></label>
                        <input type="text" placeholder="Last Name" pattern="^[^±!@£$%^&*_+§¡€#¢§¶•ªº«\\/<>?:;|=.,]{1,20}$" title="Last name is required and connot contain illegal characters" name="last" required>
                    </div>
                    <div class="field">
                        <label for="email"></label>
                        <input type="email" placeholder="Email" title="Valid email required" name="email" required>
                    </div>
                    <div class="field">
                        <label for="phone"></label>
                        <input type="tel" pattern="^04[0-9]{8}" title="Must enter an Australian mobile phone number eg 0412345678" placeholder="Phone" name="phone" required>
                    </div>
                    <div class="ui buttons">
                        <a class="ui icon button" href="cart.php">
                            <i class="ui left chevron icon"></i>
                            Back to Cart
                        </a>
                        <button class="ui green icon button"
                            style="<?php echo count($_SESSION[CART_KEY]) == 0 ? "display:none" : ""; ?>">
                            <i class="ui cart icon"></i>
                            Checkout
                        </button>
                    </div>
                </form>
            </div>

            <!-- Footer -->
            <?php require('Fragments/footer.html') ?>
        </section>
        <script type="text/javascript" src="Scripts/checkout.js"></script>
    </body>
</html>

