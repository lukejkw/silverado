<!-- Common meta tags-->
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta NAME="ROBOTS" CONTENT="index, follow">
<meta charset="utf-8"><!-- html5 version of http-equiv="Content-Type"... -->

<!-- Facebook sharing -->
<meta property="og:url" content="titan.csit.rmit.edu.au/~s1234567/wp/a1/">
<meta property="og:image" content="Images/silverado-logo150.png">
<meta property="og:description" content="Silverado movie house">
<meta property="og:title" content="Silverado movie house">
<meta property="og:site_name" content="Silverado movie house">
<meta property="og:see_also" content="lukejkw@wordpress.com">
<!-- Twitter sharing -->
<meta name="twitter:card" content="Silverado movie house">
<meta name="twitter:url" content="titan.csit.rmit.edu.au/~s1234567/wp/a1/">
<meta name="twitter:title" content="Silverado movie house">
<meta name="twitter:description" content="Silverado movie house">
<meta name="twitter:image" content="Images/silverado-logo150.png">