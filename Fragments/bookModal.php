<!-- Modal HTML -->
<div id="book-movie-modal" class="ui basic modal">
    <i class="close icon"></i>
    <div class="header">
        Book Movie
    </div>
    <div class="content">
        <form id="book-movie-form" class="ui form"
            method="POST" action="movies.php">
            <h3 class="pheader">
                Movie Information
            </h3>
            <div class="field">
                <p>Movie Name</p>
                <select id="movie-select" name="movie-id" class="ui dropdown" required>
                    <option value="">Please select a movie</option>
                    
                    <?php 
                    $movies = Movie::GetMoviesFromFile();
                    
                    for($i=0; $i < count($movies);$i++)
                        echo "<option data-code=\"" . $movies[$i]->Code . "\" 
                                value=\"" . $movies[$i]->ID . "\">" . 
                                    $movies[$i]->MovieName . 
                                "</option>";
                    
                    ?>
                    
                    <!-- Old selects
                    <option value="AF">IN THE HEART OF THE SEA</option>
                    <option value="AC">THE LAST WITCH HUNTER</option>
                    <option value="AC">HUNGER GAMES: MOCKING JAY (PART 2)</option>
                    <option value="CH">ALVIN AND THE CHIPMUNKS: THE ROAD CHIP</option>-->
                    
                </select>
                <input type="hidden" id="hdnMovieName" name="movie">
            </div>
            <div class="two field">
                <div class="field">
                    <p>Day</p>
                    <select name="day" class="ui dropdown calcTotals" required>
                        <option value="">Please select a day</option>
                        <option value="MONDAY">Monday</option>
                        <option value="TUESDAY">Tuesday</option>
                        <option value="WEDNESDAY">Wednesday</option>
                        <option value="THURSDAY">Thurday</option>
                        <option value="FRIDAY">Friday</option>
                        <option value="SATURDAY">Saturday</option>
                        <option value="SUNDAY">Sunday</option>
                    </select>
                </div>
                <div class="field">
                    <p>Time</p>
                    <select name="time" class="ui dropdown calcTotals" required>
                        <option value="">Please select a DAY first</option>
                    </select>
                </div>
            </div>
            <h3 class="header">
                Ticket Information
            </h3>
            <table class="ui inverted stackable fluid table">
                <thead>
                    <tr>
                        <th>
                            Ticket Type
                        </th>
                        <th>
                            Quantity
                        </th>
                        <th>
                            Subtotal
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <label>Adult</label>
                        </td>
                        <td>
                            <input id="adult-num" type="number" min="0" class="calcTotals">
                        </td>
                        <td>
                            <label id="adult-total">$0.00</label>
                            <input name="SA" type="hidden">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Concession</label>
                        </td>
                        <td>
                            <input id="concession-num" type="number" min="0" class="calcTotals">
                        </td>
                        <td>
                            <label id="concession-total">$0.00</label>
                            <input name="SP" type="hidden">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Child</label>
                        </td>
                        <td>
                            <input id="child-num" type="number" min="0" class="calcTotals">
                        </td>
                        <td>
                            <label id="child-total">$0.00</label>
                            <input name="SC" type="hidden">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>First Class Adult</label>
                        </td>
                        <td>
                            <input id="fc-adult-num" type="number" min="0" class="calcTotals">
                        </td>
                        <td>
                            <label id="fc-adult-total">$0.00</label>
                            <input name="FA" type="hidden">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>First Class Child</label>
                        </td>
                        <td>
                            <input id="fc-child-num" type="number" min="0" class="calcTotals">
                        </td>
                        <td>
                            <label id="fc-child-total">$0.00</label>
                            <input name="FC" type="hidden">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Beanbag - 1 Person</label>
                        </td>
                        <td>
                            <input id="beanbag-1-num" type="number" min="0" class="calcTotals">
                        </td>
                        <td>
                            <label id="beanbag-1-total">$0.00</label>
                            <input name="B1" type="hidden">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Beanbag - 2 Person</label>
                        </td>
                        <td>
                            <input id="beanbag-2-num" type="number" min="0" class="calcTotals">
                        </td>
                        <td>
                            <label id="beanbag-2-total">$0.00</label>
                            <input name="B2" type="hidden">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Beanbag - 3 Person</label>
                        </td>
                        <td>
                            <input id="beanbag-3-num" type="number" min="0" class="calcTotals">
                        </td>
                        <td>
                            <label id="beanbag-3-total">$0.00</label>
                            <input name="B3" type="hidden">
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="text-align:right;">
                            <label>Total Price</label>
                        </td>
                        <td>
                            <input type="hidden" name="total" id="hdnTotal">
                            <label id="total-label">$ 0.00</label>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>
    </div>
    <div class="actions">
        <div class="ui basic inverted red close button">
            Cancel
        </div>
        <button type="submit" 
            form="book-movie-form" 
            class="ui basic inverted green icon button">
            <i class="ticket icon"></i>
            Book movie
        </button>
    </div>
</div>
<div id="ticket-modal" class="ui basic modal">
    <i class="close icon"></i>
    <div class="header">
        You forgot to select your tickets, silly!
    </div>
    <div class="content">
        <p>
            We see that you have not selected any tickets. Please select your ticket types before continuing.
        </p>
    </div>
    <div class="actions">
        <div class="ui basic inverted icon bookIt button">
            <i class="back icon"></i>
            OK cool 
        </div>
    </div>
</div>
<script type="text/javascript" src="Scripts/bookModal.js"></script>