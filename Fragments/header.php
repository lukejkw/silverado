<?php
// Include logging
include_once("Components/Logs.php");

?>
<!-- Header -->
<div id="nav-bar" class="ui fluid stackable inverted menu">
    <!-- Logo -->
    <div id="main-logo" class="menu">
        <a class="item" href="index.php">
            <img class="ui tiny image" 
                title="Silverado nav logo image"
                src="Images/silverado-logo105.png">
        </a>
    </div>
    
    <!-- Page Links -->
    <a id="home-item" href="index.php" class="item">
        Home
    </a>
    <a id="movie-item" href="movies.php" class="item">
        Now Showing
    </a>
    <a id="about-item" href="about.php" class="item">
        Where
    </a>
    
    <!-- <a> Launches contact modal -->
    <a id="contactUs" href="#" class="contactClick ui item">
        Contact Us
    </a>
        
    <!-- Sharing buttons and actions -->
    <div class="right menu">
        
        <!-- Cart Icon with num items -->
        <a id="cart-item" href="cart.php" class="ui item">
            <i class="cart icon"></i>
                <?php echo "Cart (" . count($_SESSION[CART_KEY]) . ")" ?>
        </a>
        
        <a  href="#" class="ui item showPricing">
            <i class="dollar icon"></i>
        </a>
        
        <a  href="https://lukejkw@bitbucket.org/lukejkw/silverado" 
            title="Go to the public repo for this assignment" target="_blank" 
            class="ui item">
            <i class="bitbucket icon"></i>
        </a>
        
    </div>
</div>

<?php

// Outputs user messages
LogOutput();

?>

<!-- Include modal HTML and JS -->
<?php require('contactModal.html') ?>
<?php require('pricingModal.html')?>