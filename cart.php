<?php 

// Includes
require_once("Models/Booking.php");// Include php class before starting session so that serialization can occur
require_once("Components/Common.php");
require_once("Components/ProcessCart.php"); // Process voucher if need be.

// Process cart
ProcessCart();

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Common meta -->
        <?php require('Fragments/meta.php') ?>
        <!-- Page specific meta -->
        <meta name="description" content="desc">
        <meta name="keywords" content="silverado,movies,new">
        <link rel="shortcut icon" href="favicon.ico">
        <title>Silverado movie house</title>
        
        <!-- Client Resources -->
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="Styles/semantic.min.css"><!-- UI library http://semantic-ui.com/ -->
        <link type="text/css" rel="stylesheet" href="Styles/global_styles.css">
        <link type="text/css" rel="stylesheet" href="Styles/movies.css">
        <!-- Scripts -->
        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script><!-- JQuery JS library -->
        <script type="text/javascript" src="Scripts/semantic.min.js"></script><!-- UI library http://semantic-ui.com/ -->
    </head>
    <body>
        <section>            
            <!-- Header -->
            <?php require('Fragments/header.php') ?>
            
            <!-- Content -->
            <h2 class="ui top attached inverted header">
                <i class="film icon"></i>
                <div class="content">
                    Cart
                    <div class="sub header">All your pending bookings</div>
                </div>
            </h2>
            <div id="main-body" style="clear:both;" class="ui attached inverted segment">
                
                <?php 
                // Outputs a table of your bookings to screen
                Booking::EchoBookingTable($_SESSION[CART_KEY]);
                
                ?>
                <form method="POST" action="cart.php">
                    <table class="ui inverted fluid table">
                        <tr>
                            <td colspan="4" style="text-align:right;">
                                Total: <strong>$ <?php echo CalculateTotal() ?>.00</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="ui icon input">
                                    <input type="text" 
                                        title="Invalid voucher code format" 
                                        pattern="^([0-9]{5})-([0-9]{5})-([A-Z]{2})$" 
                                        placeholder="Voucher code" 
                                        name="voucher-code" 
                                        value = '<?php echo GetCurrentDiscount()?>'>
                                    <i class="inverted circular gift icon"></i>
                                </div>
                            </td>
                            <td style="text-align:right;">
                                
                                <a href="clear.php" class="ui basic red icon button">
                                    <i class="ui delete icon"></i>
                                    Clear Cart
                                </a>
                                
                                <a href="movies.php" class="ui icon button">
                                    <i class="ui ticket icon"></i>
                                    Add Another Ticket
                                </a>
                                
                                <!-- Button only visable if there are bookings -->
                                <button class="ui green icon button" 
                                    style="<?php echo count($_SESSION[CART_KEY]) == 0 ? "display:none" : ""; ?>">
                                    <i class="ui right chevron icon"></i>
                                    Proceed to Checkout
                                </button>
                                
                            </td>
                        </tr>
                    </table>
                </form>
                
            </div>
            <!-- Footer -->
            <?php require('Fragments/footer.html') ?>
        </section>
        <script type="text/javascript" src="Scripts/cart.js"></script>
    </body>
</html>