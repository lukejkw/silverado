$(function(){
    init();
    
    function init(){
        // Clear selected menu nodes
        $('#nav-bar .item').removeClass('active');
        
        // Select page node as active
        $('#home-item').addClass('active');
        
        // Init slider/banner
        $('.banner').unslider({
            autoplay: true, 
            infinite: true 
        });
    };
});