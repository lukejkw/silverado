$(function(){
    init();
    
    function init(){
        // Clear selected menu nodes
        $('#nav-bar .item').removeClass('active');
        // Select page node as active
        $('#about-item').addClass('active');
    };
});