$(function(){
    init();
    
    function init(){
        // Clear selected menu nodes
        $('#nav-bar .item').removeClass('active');
        
        // Select page node as active
        $('#movie-item').addClass('active');
        
        // Enable embed controls
        $('.ui.embed').embed({ 
            autoplay: false,
            modestbranding : false
        });
    };
});