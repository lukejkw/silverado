$(function () {
    // Prototypes
    String.prototype.moneyToFloat = function() {
        // Unformats money to a floating point number
        return parseFloat(this.replace("$ ",""));
    };
    Number.prototype.floatToMoney = function() {
        // Formats floats to a currency
        return "$ "+ this.toFixed(2);
    };
    
    // Declare vars
    var $form = $('#book-movie-form');
    var $modal = $('#book-movie-modal.ui.modal');
    var $day = $('select[name=day]');
    var $time = $('select[name=time]');
    var $totalLabel = $('#total-label');
    
    // JQuery Event Listerners
    //// Launch book form when .bookIt clicked
    $('.bookIt').click(function (event) {
        // Get sender
        var $sender = $(event.target);
        
        // Get clicked movie card
        var movieDivId = $sender.closest('.card').attr('id');
        if(typeof movieDivId !== 'undefined') {
            var movieId = movieDivId.substring(6);
            
            // Select appropriate movie
            $('#movie-select').val(movieId);
        }
        
        $modal.modal('show');
    });
    $day.change(setTimes); // Change time options when day is changed
    $('.calcTotals').change(calculateTotals);
    $form.submit(validateForm); // validate form before submit
    
    
    // Functions
    function validateForm(){
        // Trigger calc so that values are current
        calculateTotals();  
        
        // Get total float
        var totalFloat = getSafeFloat(
                $totalLabel.text().moneyToFloat()
            );
        
        // Check for 0 total
        if(totalFloat <= 0.00)
        {
            // Alert user
            $('#ticket-modal').modal('show');
            // Cancel submit
            return false;
        }
    };
    
    function getSafeFloat(str){
        // return safe float value based on string
        // - for some reason "" is considered a number 
        // but is not safe to parse as a float
        return (!isNaN(str) && str !== "")
            ? parseFloat(str)
            : 0.00;
    };
    
    function calculateTotals() {
        var total = 0.00;
        
        // Calc adult
        var $adultNum = $('#adult-num'); // Get num entered elem
        var $adultTotal = $('#adult-total'); // Get total entered elem
        var $adultValue = $('input[name=SA]'); // Get value entered elem
        total += calcAndSet("SA", $adultNum, $adultTotal, $adultValue); // Calculate
        
        // Calc child
        var $childNum = $('#child-num');
        var $childTotal = $('#child-total');
        var $childValue = $('input[name=SC]');
        total += calcAndSet("SC", $childNum, $childTotal, $childValue);
        
        // Calc concession
        var $concNum = $('#concession-num');
        var $concTotal = $('#concession-total');
        var $concValue = $('input[name=SP]');
        total += calcAndSet("SP", $concNum, $concTotal, $concValue);
        
        // Calc first class adult
        var $fcAdultNum = $('#fc-adult-num');
        var $fcAdultTotal = $('#fc-adult-total');
        var $fcAdultValue = $('input[name=FA]');
        total += calcAndSet("FA", $fcAdultNum, $fcAdultTotal, $fcAdultValue);
        
        // Calc first class child
        var $fcChildNum = $('#fc-child-num');
        var $fcChildTotal = $('#fc-child-total');
        var $fcChildValue = $('input[name=FC]');
        total += calcAndSet("FC", $fcChildNum, $fcChildTotal, $fcChildValue);
        
        // Calc beanbags
        var $bean1Num = $('#beanbag-1-num');
        var $bean1Total = $('#beanbag-1-total');
        var $bean1Value = $('input[name=B1]');
        var $bean2Num = $('#beanbag-2-num');
        var $bean2Total = $('#beanbag-2-total');
        var $bean2Value = $('input[name=B2]');
        var $bean3Num = $('#beanbag-3-num');
        var $bean3Total = $('#beanbag-3-total');
        var $bean3Value = $('input[name=B3]');
        
        total += calcAndSet("B1", $bean1Num, $bean1Total, $bean1Value);
        total += calcAndSet("B2", $bean2Num, $bean2Total, $bean2Value);
        total += calcAndSet("B3", $bean3Num, $bean3Total, $bean3Value);
        
        // Set total label to total
        $totalLabel.text(total.floatToMoney());
        $('#hdnTotal').val(total); // Set hidden total
        
        var movieName = $("#movie-select option[value='"+ $('#movie-select').val() + "']").text();
        $('#hdnMovieName').val(movieName);
    };
    
    function calcAndSet(code, numElem, totalElem, valElem) {
        // Get safe num 
        var numVal = getSafeFloat(numElem.val());
        // Turn negatives positive - if input somehow allows it
        if(numVal < 0)
            numVal = numVal * -1;
        // Calc total
        var total = numVal * getRate($day.val(), $time.val(), code);
        totalElem.text(total.floatToMoney()); // Set label
        valElem.val(numVal); // Set hidden input to number of tickets
        
        return total;
    }
    
    function getRate(day, time, code){
        // Declare and init variables
        var minVal = 0.00;
        var maxVal = 0.00;
        
        // Set min and max values per code type
        if(code.indexOf('B') !== -1)
        {
            // Beanbag - spec seems to indicate that all are the same price???
            minVal = 20.00;
            maxVal = 30.00;
        }
        else if(code === "FC")
        {
            // First Class Child
            minVal = 20.00;
            maxVal = 25.00;
        }
        else if(code === "FA")
        {
            // First Class Adult
            minVal = 25.00;
            maxVal = 30.00;
        }
        else if(code === "SC")
        {
            // Child
            minVal = 8.00;
            maxVal = 12.00;
        }
        else if(code === "SP")
        {
            // Concession
            minVal = 10.00;
            maxVal = 15.00;
        }
        else if(code === "SA")
        {
            // Adult
            minVal = 12.00;
            maxVal = 18.00;
        }
        
        // Return apporiate price based on day and time
        switch(day)
        {
            case "MONDAY":
            case "TUESDAY":
                return minVal;
            case "WEDNESDAY":
            case "THURSDAY":
            case "FRIDAY":
                // Check if discount time 
                if (time == "1pm" || time == "12pm")
                    return minVal;
                else
                    return maxVal
            default: 
                // Catch Sat and Sun
                return maxVal;
        }
    }
    
    function setTimes(){
        // Declare vars
        var day = $day.val();
        var times  = [{ value: "", text: "Please select a DAY first" }];
        
        // Set times based on day selected
        switch (day) {
            case "MONDAY":
            case "TUESDAY":
            case "WEDNESDAY":
            case "THURSDAY":
            case "FRIDAY":
                times = [
                    { value: "", text: "Please select a time" },
                    { value: "1pm", text: "1pm" },
                    { value: "6pm", text: "6pm" },
                    { value: "9pm", text: "9pm" }
                ];
                break;
            case "SATURDAY":
            case "SUNDAY":
                times = [
                    { value: "", text: "Please select a time" },
                    { value: "12pm", text: "12pm" },
                    { value: "3pm", text: "3pm" },
                    { value: "6pm", text: "6pm" },
                    { value: "9pm", text: "1pm" }
                ];
                break;
        }
        
        // Remove old items
        $time.children('option').remove();
        
        // Add appropriate new items items
        $.each(times, function (i, item) {
            $time.append($('<option>', {
                value: item.value,
                text: item.text
            }));
        });
    };
});