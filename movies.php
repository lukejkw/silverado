<?php 


require_once("Models/Booking.php");

require_once("Components/Common.php");

// POST code listener for book form
require_once("Components/ProcessBooking.php");

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Common meta -->
        <?php require('Fragments/meta.php') ?>
        <!-- Page specific meta -->
        <meta name="description" content="desc">
        <meta name="keywords" content="silverado,movies,new">
        <link rel="shortcut icon" href="favicon.ico">
        <title>Silverado movie house</title>
        
        <!-- Client Resources -->
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="Styles/semantic.min.css"><!-- UI library http://semantic-ui.com/ -->
        <link type="text/css" rel="stylesheet" href="Styles/global_styles.css">
        <link type="text/css" rel="stylesheet" href="Styles/movies.css">
        <!-- Scripts -->
        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script><!-- JQuery JS library -->
        <script type="text/javascript" src="Scripts/semantic.min.js"></script><!-- UI library http://semantic-ui.com/ -->
    </head>
    <body>
        <section>
            
            <!-- Header -->
            <?php require('Fragments/header.php') ?>
            
            <!-- Content -->
            <h2 class="ui top attached inverted header">
                <i class="film icon"></i>
                <div class="content">
                    Now Showing
                    <div class="sub header">Movies that you can see TODAY!</div>
                </div>
            </h2>
            <div id="main-body" class="ui attached inverted segment">
                
                <!-- Movie cards -->
                <?php
                include_once("Models/Movie.php");
                
                // Get movies from file
                $movies = Movie::GetMoviesFromFile();
                
                // Output HTML + JS required
                EchoMovieCards($movies);
                EchoMovieModals($movies);
                
                ?>
                
            </div>

            <!-- Booking Modal Fragments -->
            <?php require('Fragments/bookModal.php') ?>

            <!-- Footer -->
            <?php require('Fragments/footer.html') ?>
        </section>
        <script type="text/javascript" src="Scripts/movies.js"></script>
    </body>
</html>

