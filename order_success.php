<?php 

require("Components/Common.php");
require("Models/Order.php");

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Common meta -->
        <?php require('Fragments/meta.php') ?>
        <!-- Page specific meta -->
        <meta name="description" content="desc">
        <meta name="keywords" content="silverado,movies,new">
        <link rel="shortcut icon" href="favicon.ico">
        <title>Silverado movie house</title>
        
        <!-- Client Resources -->
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="Styles/semantic.min.css"><!-- UI library http://semantic-ui.com/ -->
        <link type="text/css" rel="stylesheet" href="Styles/global_styles.css">
        <link type="text/css" rel="stylesheet" href="Styles/movies.css">
        <!-- Scripts -->
        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script><!-- JQuery JS library -->
        <script type="text/javascript" src="Scripts/semantic.min.js"></script><!-- UI library http://semantic-ui.com/ -->
    </head>
    <body>
        <section>            
            <!-- Header -->
            <?php require('Fragments/header.php') ?>
            
            <!-- Content -->
            <h2 class="ui top attached inverted header">
                <i class="film icon"></i>
                <div class="content">
                    Order success
                    <div class="sub header">Notification of success</div>
                </div>
            </h2>
            <div id="main-body" class="ui attached inverted segment">
                
                <h2>
                    Order successful
                </h2>
                <p>
                    Your order was successfully placed.
                </p>
                <p>
                    You will need to pay for you bookings when arriving at the cinema, so please arrive a little earlier than your movie start time to pay.
                </p>
                
                <div class="ui buttons">
                    
                    <a class="ui basic button" href="index.php">
                        Back to home page
                    </a>
                    <a class="ui green button" target="_blank" href="<?php echo GetRecieptPath() ?>">
                        Download Order File
                    </a>
                    
                </div>
            </div>

            <!-- Footer -->
            <?php require('Fragments/footer.html') ?>
        </section>
    </body>
</html>

