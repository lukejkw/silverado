<?php
/*
 * Gallary code
 */
define("GALLERY_IMAGE_DIR", "Images/Cinema");

function EchoGallery()
{   
    // Get files from managed file (avoid having to hand code image srcs)
    $files = scandir(GALLERY_IMAGE_DIR, SCANDIR_SORT_DESCENDING);
    
    // Scripts + CSS
    echo "<script src='Components/unite-gallery/js/unitegallery.min.js' type='text/javascript'  ></script>";
    echo "<link  href='Components/unite-gallery/css/unite-gallery.css' rel='stylesheet' type='text/css' />";
    //// Theme stuff
    echo "<script src='Components/unite-gallery/themes/default/ug-theme-default.js' type='text/javascript'></script>";
    echo "<link  href='Components/unite-gallery/themes/default/ug-theme-default.css' rel='stylesheet' type='text/css' />";
    
    // Gallery HTML
    echo "<div id=\"cinema-gallery\">";
    for($i = 0; $i < count($files); $i++)
    {
        // Check for local and parent reference
        if( $files[$i] != "." && $files[$i] != "..")
        {
            // Build up file path
            $filePath = GALLERY_IMAGE_DIR . "/" . $files[$i];
            // Output nodes for each image in file
            echo "<img alt=\"Image " . $i ."\" src=\"" . $filePath . "\" data-image=\"" . $filePath . "\">";   
        }
    }
    echo "</div>";
    
    // Activator script
    echo "<script type=\"text/javascript\"> $(function(){ $('#cinema-gallery').unitegallery();}); </script>";
}

?>
