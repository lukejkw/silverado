<?php 

require_once("Models/Booking.php");
require_once("Models/Order.php");
require_once("Components/Logs.php");
require_once("Components/ProcessCart.php"); 


function ProcessCheckout()
{
    // See if posted back to self
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $first = $_POST["first"];
        $last = $_POST["last"];
        $phone = $_POST["phone"];
        $email = $_POST["email"];
        
        // Validate data
        if(IsEmpty($first) || IsEmpty($last) || IsEmpty($phone) || IsEmpty($email))
        {
            LogError("Please ensure that you have entered all the required inforamtion and try again");
            return;
        }
        
        // Create order
        $fullName = $first . " " .$last;
        $total = CalculateTotal();
        $discount = CalculateDiscount();
        $order = new Order($fullName, $phone, $email, $total, $discount, Booking::GetBookings());
        
        // Write order
        SaveOrder($order);
        
        LogMessage("Order Files generated for #". $order->OrderId);
        
        // Clear session data
        Booking::ClearBookings();
        ClearDiscount();
        
        // Redirect to order success page
        $host  = $_SERVER['HTTP_HOST'];
        $uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $extra = 'order_success.php';

        header("Location: http://$host$uri/$extra");
        
        exit;
    }
}

function IsEmpty($value)
{
    return (!isset($value) || trim($value) === '');
}

?>