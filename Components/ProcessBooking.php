<?php

// Include logging
include_once("Components/Logs.php");

// See if posted back to self
if($_SERVER['REQUEST_METHOD'] == 'POST')
{   
    $movieId = $_POST["movie-id"];
    $movieName = $_POST["movie"];
    $day = $_POST["day"];
    $time = $_POST["time"];
    $adultNum = $_POST["SA"];
    $concNum = $_POST["SP"];
    $childNum = $_POST["SC"];
    $fcAdult = $_POST["FA"];
    $fcChild = $_POST["FC"];
    $bean1 = $_POST["B1"];
    $bean2 = $_POST["B2"];
    $bean3 = $_POST["B3"];
    $total = (int)$_POST["total"];

    // Note/TODO
    // I know I should really be calculating the ticket price server side 
    // (I am trusting the JS). This would be a security flaw in a production app but
    // I dont have the time :(.
    
    // Create Booking Object
    $booking = new Booking(
        $movieId,
        $movieName,
        $day, 
        $time, 
        $adultNum, 
        $concNum, 
        $childNum, 
        $fcAdult, 
        $fcChild,
        $bean1,
        $bean2,
        $bean3,
        $total);
    
    // Save in session
    Booking::SaveBooking($booking);
    
    // Log
    LogMessage("Booking successfully added to cart");
}


?>