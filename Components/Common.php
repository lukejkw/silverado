<?php
/*
 * This file contains common code shared accross the application 
 */

// Constants
define("APP_NAME", "Silverado");
define("CART_KEY", "SILVERADO_CART");

session_start();

// Init booking array in session
if(!isset($_SESSION[CART_KEY]))
    $_SESSION[CART_KEY] = array();

?>