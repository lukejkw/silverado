<?php // This Component writes errors and other messages to screen

// Constants
define("ERROR_KEY", "SILVERADO_ERRORS");
define("MESSAGE_KEY", "SILVERADO_MESSAGES");
define("LOG_FILE_PATH", "Data/logs.txt");

// Outputs all logs to screen
function LogOutput()
{
    if(isset($_SESSION[ERROR_KEY]))
    {
        // Get errors
        $messages = $_SESSION[ERROR_KEY];
        for($i = 0; $i < count($messages); $i++)
        {
            // Output to screen
            echo "<div class='ui negative icon message'>".
                    "<i class='warning circle icon'></i>".
                    "<div class='content'>
                        <div class='header'>
                            Oops! Seems there was a problem
                        </div>
                        <p>".
                            $messages[$i].
                        "</p>
                    </div>
                </div>";
        }
         // Wipe logs
        $_SESSION[ERROR_KEY] = array();
    }
    
    if(isset($_SESSION[MESSAGE_KEY]))
    {
        // Get info messages
        $messages = $_SESSION[MESSAGE_KEY];
        for($i = 0; $i < count($messages); $i++)
        {
            // Output to screen
            echo "<div class='ui success icon message'>".
                "<i class='info icon'></i>".
                "<div class='content'><div class='header'>Success!</div><p>".
                    $messages[$i].
                 "</p></div></div>";
        }
        
        // Wipe logs
        $_SESSION[MESSAGE_KEY] = array();
    }
}

// Adds an error to the error log
function LogError($errorMessage)
{
    // Add to log
    if(!isset($_SESSION[ERROR_KEY]))
        $_SESSION[ERROR_KEY] = array($errorMessage);
    else
        array_push($_SESSION[ERROR_KEY], $errorMessage);
        
    LogToFile("ERROR", $errorMessage);
}

// Adds an info message to the log
function LogMessage($message)
{
    // Add to log
    if(!isset($_SESSION[MESSAGE_KEY]))
        $_SESSION[MESSAGE_KEY] = array($message);
    else
        array_push($_SESSION[MESSAGE_KEY], $message);
        
    LogToFile("USER NOTICE", $message);
}

// Writes all logs to file
function LogToFile($type, $log)
{
    // Open file for appending
    $file = fopen(LOG_FILE_PATH, "a");
    
    // Check if successful
    if(!isset($file))
        throw new Exception("Cannot open log file!", 1);
        
    // Write log to file
    $fullLog = $type . ": " . $log . ". Logged On:" . time() . "\n";
    fwrite($file, $fullLog);
    
    // Close file
    fclose($file);
}

function OutputFileLogs()
{
    // Open file for appending
    $file = fopen(LOG_FILE_PATH, "r");
    
    // Check if successful
    if(!isset($file))
        throw new Exception("Cannot open log file!", 1);
        
    // Read file
    while(!feof($file))
    {
        $line = fgets($file, 255);
        // Output to screen
        echo "<div class='ui message'>".
                $line.
                "</div>";
    }
    
    // Close file
    fclose($file);
}

?>
