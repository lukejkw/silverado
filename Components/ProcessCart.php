<?php 

// Include Regex utils
include_once("Components/RegexUtils.php");
include_once("Components/Logs.php");

define("DISCOUNT_KEY", "DISCOUNT");
define("VOUCHER_KEY", "CURRENT_VOUCHER");

function ProcessCart()
{
    // Check if POST Request
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {   
        // Scrape voucher data
        $voucherCode = trim($_POST["voucher-code"]);
        
        
        // Check if user entered a voucher code
        if(isset($_POST["voucher-code"]) && $voucherCode !== '')
        {
            // Validate voucher code
            if(CheckVoucherCode(trim($voucherCode)))
            {
                // Voucher valid
                LogMessage("Your voucher was successfully applied! You get 20 % off");
                
                // Set voucher flag
                $_SESSION[DISCOUNT_KEY] = true;
                
                $_SESSION[VOUCHER_KEY] = trim($voucherCode);
                
                // Redirect to checkout
                $host  = $_SERVER['HTTP_HOST'];
                $uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                $extra = 'checkout.php';
                header("Location: http://$host$uri/$extra");
            }
            else 
            {
                ClearDiscount();
                LogError("Voucher code invalid! No discount has been applied. "
                        ."Please double check the voucher code and try again.");
                return;
            }
        }
        else 
        {
            ClearDiscount();
            
            // No voucher code - Redirect to checkout
            $host  = $_SERVER['HTTP_HOST'];
            $uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
            $extra = 'checkout.php';
            header("Location: http://$host$uri/$extra");
        }
    }
}

function DiscountAvailable()
{
    if(!isset($_SESSION[DISCOUNT_KEY]))
        return false;
        
    return $_SESSION[DISCOUNT_KEY] == true;
}

function ClearDiscount()
{
    // Clear values for discount
    $_SESSION[DISCOUNT_KEY] = false;
    $_SESSION[VOUCHER_KEY] = "";
}

function GetCurrentDiscount()
{
    if(isset($_SESSION[VOUCHER_KEY]))
        return $_SESSION[VOUCHER_KEY];
    return "";
}

function CalculateTotal()
{
    $total = 0;
    
    $bookings = $_SESSION[CART_KEY];
    
    for($i = 0; $i < count($bookings); $i++)
    {
        // Add up total
        $total = $total + $bookings[$i]->Total;
    }
    
    // Apply discount
    if(DiscountAvailable())
        $total *= 0.80;
    
    return number_format($total, 0);
}

function CalculateDiscount()
{
    $total = 0.00;
    
    if(DiscountAvailable())
    {
        $bookings = $_SESSION[CART_KEY];
        for($i = 0; $i < count($bookings); $i++ )
        {
            // Add up total
            $total = $total + floatval($bookings[$i]->Total);
        }
        $total = $total * 0.20;
    }
    
    return number_format($total, 0);
}

?>