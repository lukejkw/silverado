<?php

define("VOUCHER_REGEX", "/^([0-9]{5})-([0-9]{5})-([A-Z]{2})$/");

    
function CheckVoucherCode($input)
{
    // First, check if voucher code matches regex pattern
    if(!preg_match(VOUCHER_REGEX, $input))
        return false;
        
        echo $input;
        
    // Get 3 substrings
    $codearray = explode("-", $input);
    
    print_r($codearray);
    
    // Check if checksum appropriate
   return (GetCheckChar($codearray[0]) == $codearray[2][0] 
            && GetCheckChar($codearray[1]) == $codearray[2][1]);
}

function GetCheckChar($intArray)
{
    $ALPHABET =  ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", 
        "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    
    // Calc check value
    $digitNum = ( ($intArray[0] * $intArray[1] + $intArray[2] ) 
        * $intArray[3] + $intArray[4] ) % 26;
    
    echo $digitNum;
    
    // Return approriate CHAR value
    return $ALPHABET[$digitNum];
}

function TestRegexCheckCode()
{
    /* 
     * Simple code to test that my regext logic works.
     * I would usually write a unit test for this but that is out of scope
     */
    // Passes
    echo "Valid: " . (int)CheckVoucherCode("12345-67890-ZI") . "<br />";
    echo "Valid: " . (int)CheckVoucherCode("74108-52963-IN") . "<br />";
    echo "Valid: " . (int)CheckVoucherCode("30515-19057-KA") . "<br />";
    echo "Valid: " . (int)CheckVoucherCode("15415-84120-OO") . "<br />";
    echo "Valid: " . (int)CheckVoucherCode("20786-19867-KF") . "<br />";

    // Fails
    echo "Invalid: " . (int)CheckVoucherCode("12-354-AB") . "<br />";
    echo "Invalid: " . (int)CheckVoucherCode("12345 6789 NA") . "<br />";
    echo "Invalid: " . (int)CheckVoucherCode("10551-305149-FU") . "<br />";
    //// Match regex but not check pattern
    echo "Invalid: " . (int)CheckVoucherCode("85261-12757-OK") . "<br />";
    echo "Invalid: " . (int)CheckVoucherCode("48151-62342-LO") . "<br />";
}

?>