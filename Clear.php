<?php 

require_once("Components/Common.php");

include("Components/Logs.php");

// Clear bookings
if(isset($_SESSION[CART_KEY]))
    $_SESSION[CART_KEY] = array();

LogMessage("Your cart has been cleared of all bookings");

/* 
 * Redirect to cart
 */
$host  = $_SERVER['HTTP_HOST'];
$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$extra = 'cart.php';  // change accordingly

header("Location: http://$host$uri/$extra");
exit;

?>