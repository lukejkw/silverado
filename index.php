<?php require("Components/Common.php") ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Common meta -->
        <?php require('Fragments/meta.php') ?>
        <!-- Page specific meta -->
        <meta name="description" content="desc">
        <meta name="keywords" content="silverado,movies,new">
        <link rel="shortcut icon" href="favicon.ico">
        <title>Silverado movie house</title>
        
        <!-- Client Resources -->
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="Styles/semantic.min.css"><!-- UI library http://semantic-ui.com/ -->
        <link type="text/css" rel="stylesheet" href="Styles/unslider.css"><!-- Unslider http://unslider.com/ -->
        <link type="text/css" rel="stylesheet" href="Styles/unslider-dots.css"> 
        <link type="text/css" rel="stylesheet" href="Styles/global_styles.css">
        <link type="text/css" rel="stylesheet" href="Styles/index.css">
        
        <!-- JQuery JS library -->
        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    </head>
    <body>
        <section>
            <!-- Header -->
            <?php require('Fragments/header.php') ?>
            
            <!-- Silder -->
            <div id="header-banner" class="ui inverted segment">
                <div class="banner">
                    <ul>
                        <li>
                            <img class="ui image" title="MovieBanner image" src="Images/marvel_banner_with_text.jpg">
                        </li>
                        <li>
                            <img class="ui image" title="MovieBanner image" src="Images/mocking_jay_banner_with_text.jpg">
                        </li>
                    </ul>
                </div>
            </div>
            
            <!-- Content -->
            <h1 class="ui top attached inverted header">
                <img class="ui tiny image" title="Silverado nav logo image" src="Images/silverado-logo105.png">
                <div class="content">
                    Silverado
                    <div class="sub header">Small cinema, big entertainment!</div>
                </div>
            </h1>
            <div id="main-body" class="ui attached inverted segment">
                
                <!-- Main Index content -->

                <!-- Intro -->
                <div class="ui inverted segment">
                    <h2 id="intro" class="centred header">
                        Silverado Cinemas - Grand Re-Opening!
                    </h2>
                    <p>
                        For the uninformed, Silverado is a small cinema that packs a big punch of entertainment! After being in the movie and cinema industry since 1986*, you could say that we know a thing or two about how to entertain but now - there is more!
                    </p>
                    <p>
                        We took some time off to 'spruce' up our theatres and now - Silverado is back with a vengence to excite our small country town once again with high quality movies!
                    </p>
                    <p>
                        OK, so we have your attension now. But you are wondering, "but how?". Well, with brand 3D projection facilities, lighting and sound provided by Dolby! That's how!
                    </p>
                    
                    <h3 class="centred header">
                        Dolby Cinema
                    </h3>
                    <img class="ui large right floated rounded image" title="Dolby Cinema" src="Images/Dolby-Cinema-seats.jpg">
                    <p>
                        "Dolby Cinema combines the most powerful image and sound technologies with inspired cinema design to transform your visit into a completely captivating cinematic event."
                    </p>
                    <p>
                        "Dolby Cinema delivers the total cinema experience, featuring Dolby Atmos and Dolby Vision technologies in our state-of-the-art theatre."
                    </p>
                    <p>
                        Read more at <a href="http://www.dolby.com/us/en/platforms/dolby-cinema.html" target="_blank">www.Dolby.com</a>.
                    </p>
                    
                    <h3 class="centred header">
                        All new Cinema Floor Plans
                    </h3>
                    <p>
                        Some of our loyal customers and expressed their views on our aging cinema floor plan and - we agreed.
                    </p>
                    <p>
                        Silverado now has: 
                    </p>
                    <ul>
                        <li>12 <em>First Class</em> Seats to cater for those bottoms that require a little more comfort than others.</li>
                        <li>40 <em>Normal</em> seats for those who just want to see a great movie. </li>
                        <li>13 <em>Beanbag</em> seats! An all new movie seating experience, for those who like a cuddle or to have a bit of a lie down while enjoying the latest in movie entertain ment </li>
                    </ul>
                    
                    <p>
                        Now we know that you are excited!
                    </p>
                    <p>
                        If you can't wait to come to the <em>Silverado Grand Re-Opening</em>, don't hesitate to pre-book your tickets now.
                    </p>
                    <p>
                        <a class="contactClick">Have any questions?</a>
                    </p>
                    
                </div>
                
                <div id="gallery-container">
                    <h2 id="intro" class="centered header">
                        Gallery of our new facilities!
                    </h2>
                    <?php 
                    
                    include('Components/GalleryUtils.php');
                    
                    EchoGallery();
                    
                    ?>
                </div>
                
                <div id="times-container">
                    <?php require('Fragments/times.html') ?>
                </div>
                
                <div class="ui stackable grid">
                    <div class="row">
                        
                        <?php require('Fragments/ticketPricing.html') ?>
                            
                    </div>
                </div>
                
                
            </div>
            
            <!-- Footer -->
            <?php require('Fragments/footer.html') ?>
        </section>
        <!-- Scripts -->
        <script type="text/javascript" src="Scripts/semantic.min.js"></script><!-- UI library http://semantic-ui.com/ -->
        <script src="Scripts/unslider-min.js"></script> <!-- Unslider http://unslider.com/ -->
        <script type="text/javascript" src="Scripts/easing.js"></script>
        <script type="text/javascript" src="Scripts/index.js"></script>
    </body>
</html>