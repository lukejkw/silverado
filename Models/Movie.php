
<?php
    define("MOVIE_FILE_PATH", "Data/movies.txt");
    define("FILE_BUFF", 1024);
    define("DELIM", ";");
    define("NUM_MOVIE_PROPERTIES", 11);
    
    class Movie
    {
        // Properties
        private static $Movies;
        
        public $ID = -1;
        public $MovieName = "";
        public $Code = "";
        public $Length = "";
        public $Rating = -1;
        public $Genre = "";
        public $Cast = "";
        public $ImageSrc = "";
        public $Desc = "";
        public $Trailer = "";
        public $TimesHtml = "";
        
        // Constructors
        function __construct(
            $id, 
            $movieName, 
            $code,
            $length, 
            $rating, 
            $genre,
            $cast,
            $imageSrc,
            $desc,
            $trailer,
            $timeHtml)
        {
            $this->ID = $id;
            $this->MovieName = $movieName;
            $this->Code = $code;
            $this->Length = $length;
            $this->Rating = $rating;
            $this->Genre = $genre;
            $this->Cast = $cast;
            $this->ImageSrc = $imageSrc;
            $this->Desc = $desc;
            $this->Trailer = $trailer;
            $this->TimesHtml = $timeHtml;
        }
        
        // Public Static methods
        public static function GetMoviesByRating($rating)
        {
            $allMovies = GetAllMovies();
            $filteredMovies = [];
            
            // Push matching movies on to stack 
            for($i - 0; count($allMovies); $i++)
                if($rating == $allMovies[$i]->Rating)
                    array_push($filteredMovies, $allMovies[$i]);
            
            return $filteredMovies;
        }
        
        public static function GetMoviesFromFile()
        {
            // Vars
            $movies = array();
            
            // Open file
            $file = fopen(MOVIE_FILE_PATH, "r");
            
            if(!isset($file))
            {
                LogError("Unable to open movie file");
                return;
            }
            
            // Read file
            while(!feof($file))
            {
                $line = fgets($file, FILE_BUFF);
                $aryProps = explode(DELIM, $line);
                
                // Check for correct num props
                if(count($aryProps) < NUM_MOVIE_PROPERTIES)
                {
                    // Log
                    LogError("Movie file corrupt. Unable to populate movies array");
                    return;
                }
                
                $movie = new Movie(
                    $aryProps[0], // ID 
                    $aryProps[1], // Name
                    $aryProps[2], // Code
                    $aryProps[3], // Length
                    $aryProps[4], // Rating
                    $aryProps[5], // Genre
                    $aryProps[6], // Cast
                    $aryProps[7], // Image Src
                    $aryProps[8], // Desc
                    $aryProps[9], // Trailer
                    $aryProps[10]);// TimesHtml
                
                array_push($movies, $movie);
            }
            
            fclose($file);
            
            return $movies;
        }
    }
    
    
    // Helper methods
    function EchoMovieCards($moviesAry)
    {
        echo "<div class=\"ui four doubling stackable cards\">";
        
        for($i = 0; $i < count($moviesAry); $i++)
        {
            // Output cards     
            echo "<div id=\"movie-" . $moviesAry[$i]->ID . "\" class=\"card\">
                    <div class=\"image\">
                        <img class=\"modal_" . $moviesAry[$i]->ID . "\" src=\"" . $moviesAry[$i]->ImageSrc . "\">
                    </div>
                    <div class=\"content\">
                        <a class=\"header\">"
                            . $moviesAry[$i]->MovieName
                        . "</a>
                        <div class=\"meta\">
                            <span class=\"date\">
                                Length: "
                                . $moviesAry[$i]->Length
                            . "</span>
                            <span class=\"date\">
                                Rating: "
                                . $moviesAry[$i]->Rating
                            . "</span>
                        </div>
                    </div>
                    <div class=\"extra content\">".
                        $moviesAry[$i]->TimesHtml.
                    "</div>
                    <div class=\"extra content\">
                        <div class=\"ui two buttons\">
                            <a href=\"#\" class=\"ui basic white inverted icon button modal_" . $moviesAry[$i]->ID . "\">
                                <i class=\"expand icon\"></i>
                                More
                            </a>
                            <a href=\"#\" class=\"ui basic white inverted icon button bookIt\">
                                <i class=\"ticket icon\"></i>
                                Book
                            </a>
                        </div>
                    </div>
                </div>";
        }
        
        echo "</div>";
    }
    
    function EchoMovieModals($moviesAry)
    {
        $modaljs = "";
        
        // Output modal HTML
        for($i = 0; $i < count($moviesAry); $i++)
        {
            echo "<div id=\"modal-". $moviesAry[$i]->ID ."\" class=\"ui basic modal\">
                <i class=\"close icon\"></i>
                <div class=\"header\">"
                    . $moviesAry[$i]->MovieName
                ."</div>
                <div class=\"image content\">
                    <div class=\"ui large image\">
                        <img src=\"" . $moviesAry[$i]->ImageSrc . "\">
                    </div>
                    <div class=\"description\">
                        <h3 class=\"header\">
                            Description
                        </h3>
                        <p>"
                            . $moviesAry[$i]->Desc
                        ."</p>
                        <h3 class=\"header\">
                            Genre
                        </h3>
                        <p>"
                            . $moviesAry[$i]->Genre
                        ."</p>
                        <h3 class=\"header\">
                            Cast
                        </h3>
                        <p>"
                            . $moviesAry[$i]->Cast
                        ."</p>
                        <h3 class=\"header\">
                            Times
                        </h3>".
                        $moviesAry[$i]->TimesHtml
                        ."
                        <h3 class=\"header\">
                            Trailer
                        </h3>
                        <div class='ui embed' data-source='youtube' data-id='". $moviesAry[$i]->Trailer . "'>
                        </div>
                        <span><em>Click/Touch video to Pause/Play</em></span>
                    </div>
                </div>
                <div class=\"actions\">
                    <div class=\"ui basic inverted red deny button\">
                        Back to movies
                    </div>
                    <div class=\"ui basic inverted green icon button bookIt\">
                        Awesome, lets book it!
                        <i class=\"ticket icon\"></i>
                    </div>
                </div>
            </div>";
            
            $modaljs = $modaljs . "$('.modal_" . $moviesAry[$i]->ID . "').click(function (){ $('#modal-" . $moviesAry[$i]->ID . "').modal('show'); });";
        }
        
        // Inject JS
        echo "<script type=\"text/javascript\">$(function(){" . $modaljs . "});</script>";
    }
?>