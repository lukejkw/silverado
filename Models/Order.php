<?php

include_once("Components/Logs.php");

define("ORDER_LOG_FILE_PATH", "Data/orders.json");
define("ORDER_FILE_PATH", "Data/Orders/");
define("LAST_RECIEPT_KEY","LAST_RECIEPT");

class Order
{
    // Properties
    public $OrderId = "";
    public $CustName = "";
    public $Phone = "";
    public $Email = "";
    public $Total = 0;
    public $Discount = 0;
    public $Bookings = [];

    // Constructor
    function __construct(
        $custName, 
        $phone, 
        $email, 
        $total,
        $discount, 
        $bookings)
    {
        $this->OrderId = time();
        $this->CustName = $custName;
        $this->Phone = $phone;
        $this->Email = $email;
        $this->Total = $total;
        $this->Discount = $discount;
        $this->Bookings = $bookings;
    }
}

// Saves the order to file
function SaveOrder($order)
{
    // Heeder
    $data = "<div style='background-color:black; color:white; width:400px; margin: 0 auto; border: 1px solid white;padding:10px'>"
             . "<h2 class='header'>"
               . "Your Silverado Movie Reservations/Tickets"
             . "</h2>"
        
    
    // Build Order header info
        . "<h3>Customer and Order Details</h3>"
        . "<p><strong>Order Number:</strong> " . $order->OrderId ."</p>"
        . "<p><strong>Name:</strong> " . $order->CustName ."</p>"
        . "<p><strong>Email:</strong> " . $order->Email . "</p>"
        . "<p><strong>Phone:</strong> " . $order->Phone . "</p>"
        . "<p><strong>Total:</strong> $" . $order->Total . ".00</p>"
        . "<p><strong>Discount:</strong> $" . $order->Discount . ".00</p>"
        . "<p><strong>Voucher:</strong> " . $_SESSION["CURRENT_VOUCHER"] . "</p>";


        // Add tickets
        $items = "";
        for($i = 0; $i < count($order->Bookings); $i++)
        {
            $data = $data . "<h4 style='border-bottom: 1px solid white;'>Ticket #" . ($i + 1) . "</h4>";
            $data = $data . $order->Bookings[$i]->ToString();
            
            // Add json items
            $items = $items . $order->Bookings[$i]->ToJson()
            . ($i == (count($order->Bookings)-1) ? "" : ","); // append a comma if not last
        }
            
        // Footer
        $data = $data . "<div><em>This ticket is meant to serve as a guideline. Silverado reserves the right to change the reservation at any time.</em></div>";
        $data = $data . "</div>";

    // Try Open order log file
    $file = fopen(ORDER_LOG_FILE_PATH, "a");
    if(!isset($file))
    {
        LogError("Unable to open order log file!");
        return;
    }
       
    // Build other json
    $orderJson = "var order-". $order->OrderId ." = {
        'OrderDetails': {
            'name': '".$order->CustName."',
            'email': '".$order->Email."',
            'phone': '".$order->Phone."',
            'total': '".$order->Total."',
            'discount': '".$order->Discount."',
            'voucher-code:' '". $_SESSION["CURRENT_VOUCHER"] ."'
        },
        items : ["
            . $items .
        "]
    };
    
    ";
    
    // Append to file
    fwrite($file, $orderJson);
    
    // Close file
    fclose($file);
    
    // Try create ticket file
    $orderRecieptPath = ORDER_FILE_PATH . $order->OrderId . ".html";
    $file = fopen($orderRecieptPath, "w");
    if(!isset($file))
    {
        LogError("Unable to create order log file!");
        return;
    }
    
    // Write to file
    fwrite($file, $data);
    
    // Close file
    fclose($file);
    
    $_SESSION[LAST_RECIEPT_KEY] = $orderRecieptPath;
}

function GetRecieptPath()
{
    if(isset($_SESSION[LAST_RECIEPT_KEY]))
        return $_SESSION[LAST_RECIEPT_KEY];
    
    LogError("Unable to find reciept for last transaction");
    
    return "#";
}

?>