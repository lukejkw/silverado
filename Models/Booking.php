<?php

include_once("Components/Common.php");

class Booking
{
    // Properties
    public $MovieId = -1;
    public $MovieName = "";
    public $Day = "";
    public $Time = "";
    public $AdultNum = 0;
    public $ConcNum = 0;
    public $ChildNum = 0;
    public $FcAdult = 0;
    public $FcChild = 0;
    public $Bean1 = 0;
    public $Bean2 = 0;
    public $Bean3 = 0;
    public $Total = 0.00;

    // Constructor
    function __construct(
        $movieId,
        $movieName,
        $day,
        $time,
        $adultNum,
        $concNum,
        $childNum,
        $fcAdult,
        $fcChild,
        $bean1,
        $bean2,
        $bean3,
        $total)
    {
        $this->MovieId = $movieId;
        $this->MovieName = $movieName;
        $this->Day = $day;
        $this->Time = $time;
        $this->AdultNum = $adultNum;
        $this->ConcNum = $concNum;
        $this->ChildNum = $childNum;
        $this->FcAdult = $fcAdult;
        $this->FcChild = $fcChild;
        $this->Bean1 = $bean1;
        $this->Bean2 = $bean2;
        $this->Bean3 = $bean3;
        $this->Total = $total;
    }
    
    // Returns a string representation of the booking 
    public function ToString()
    {
        return   "<p>Movie: ".$this->MovieName ."</p>"
                ."<p>Day: ".$this->Day."</p>"
                ."<p>Time: ".$this->Time."</p>"
                ."<p>Num Adult: ".$this->AdultNum."</p>"
                ."<p>Num Conc: ".$this->ConcNum."</p>"
                ."<p>Num Child: ".$this->ChildNum."</p>"
                ."<p>Num FC Adult: ".$this->FcAdult."</p>"
                ."<p>Num FC Child: ".$this->FcChild."</p>"
                ."<p>Num Bean 1: ".$this->Bean1."</p>"
                ."<p>Num Bean 2: ".$this->Bean2."</p>"
                ."<p>Num Bean 3: ".$this->Bean3."</p>"
                ."<p>Sub-Total: ".$this->Total ."</p>";
    }
    
    // Returns a string representation of the booking 
    public function ToJson()
    {
        return   "{Movie: '".$this->MovieName ."',"
                ."Day: '".$this->Day."',"
                ."Time: '".$this->Time."',"
                ."Num-Adult: '".$this->AdultNum."',"
                ."Num-Conc: '".$this->ConcNum."',"
                ."Num-Child: '".$this->ChildNum."',"
                ."Num-FC-Adult: '".$this->FcAdult."',"
                ."Num-FC-Child: '".$this->FcChild."',"
                ."Num-Bean-1: '".$this->Bean1."',"
                ."Num-Bean-2: '".$this->Bean2."',"
                ."Num-Bean-3: '".$this->Bean3."',"
                ."Sub-Total: '".$this->Total ."'}";
    }
    
    public static function SaveBooking($booking)
    {
        // Push into cart
        if(isset($_SESSION[CART_KEY]))
        {
            array_push($_SESSION[CART_KEY], $booking);
        }
        else 
        {
            $_SESSION[CART_KEY] = array($booking);
        }
    }
    
    public static function GetBookings()
    {
        return $_SESSION[CART_KEY];
    }
    
    public static function ClearBookings()
    {
        return $_SESSION[CART_KEY] = array();
    }
    
    public static function EchoBookingTable($bookings)
    {
        echo "<table class='ui fluid inverted stackable table'>";
            echo "<tr>
                    <th>Movie Name</th>
                    <th>Day & Time</th>
                    <th>Tickets</th>
                    <th>Total Ticket Cost</th>
                 </tr>";
        
        if(count($bookings) > 0)
        {
            for($i = 0; $i < count($bookings); $i++)
            {
                // Calc total tickets
                $tickets = "Adult: ". $bookings[$i]->AdultNum . "<br />" .
                            "Conc.: ".   $bookings[$i]->ConcNum . "<br />" .
                            "Child: ".   $bookings[$i]->ChildNum . "<br />" .
                            "First-Class Adult: ".  $bookings[$i]->FcAdult . "<br />" .
                            "First-Class Child: ".  $bookings[$i]->FcChild . "<br />" .
                            "Beanbag 1: ".  $bookings[$i]->Bean1 . "<br />" .
                            "Beanbag 2: ". $bookings[$i]->Bean2 . "<br />" .
                            "Beanbag 3: ". $bookings[$i]->Bean3;
                
                echo "<tr>
                        <td>".
                            $bookings[$i]->MovieName
                        ."</td>
                        <td>".  
                            $bookings[$i]->Day . " " . $bookings[$i]->Time
                        ."</td>
                        <td>".
                            $tickets
                        ."</td>
                        <td>".
                            "$ " . $bookings[$i]->Total. ".00"
                        ."</td>
                    </tr>";
            }
        }
        else
        {
            echo "<tr><td colspan=\"4\"><em>There are no bookings in your cart, YET!</em></td></tr>";    
        }
        
        
        echo "</table>";
    }
}

?>