------------------------------------------------------------------------------------------------------
--------------------------------- Developer Information ----------------------------------------------
------------------------------------------------------------------------------------------------------

Unit: Web Programming
Assignment: Assignment 2
Developer: Luke Warren
Contact Email: s3409172@student.rmit.edu.au
GIT Repo: lukejkw@bitbucket.com/lukejkw/silverado.git

------------------------------------------------------------------------------------------------------
------------------------------------ Notes -----------------------------------------------
------------------------------------------------------------------------------------------------------

The current iteration of the website is effectively a fully functional movie reservation site

------------------------------------------------------------------------------------------------------
------------------------------------ Features -----------------------------------------------
------------------------------------------------------------------------------------------------------

- Fully responsive
- Minimum postbacks/refreshes by using modal popups
- Responsive form inputs (eg booking form)
- Responsive Silder
- Google maps
- HTML5 Validation
- Logging and notifications built in
- Downloadbale HTML ticket files
- Voucher codes
- Order json logging (need work)
- Modal popups

------------------------------------------------------------------------------------------------------
--------------------------------- Browser Requirements -----------------------------------------------
------------------------------------------------------------------------------------------------------

'Silverado' was built using JavaScript and HTML5.

The website may not display as intended on browsers that do not meet these requirements.

------------------------------------------------------------------------------------------------------
-------------------------------------- References ---------------------------------------------------
------------------------------------------------------------------------------------------------------

UnitGallery:
http://unitegallery.net/

JQuery:
https://jquery.com/

Semantic-UI:
http://semantic-ui.com/

Unslider:
http://unslider.com/

Dolby Image:
http://www.bigpicturebigsound.com/artman2/uploads/4/Dolby-Cinema-seats.jpg

Banner image 1:
https://www.google.co.za/imgres?imgurl=http://www.comicbookmovie.com/images/users/uploads/22781/AVENGERS%252520BANNER.JPG&imgrefurl=http://www.comicbookmovie.com/fansites/SamadCBM/news/?a%3D29990&h=612&w=1280&tbnid=Iz2MdC18CXGKRM:&docid=TFH2GVOTv-NXOM&ei=-fuVVoOGF4SyacWCu2A&tbm=isch&ved=0ahUKEwjD5segoabKAhUEWRoKHUXBDgw4rAIQMwgGKAMwAw

Banner image 2:
https://www.google.co.za/imgres?imgurl=http://www.liveforfilms.com/wp-content/uploads/2014/01/hunger_games_mockingjay__part_one-banner.jpg&imgrefurl=http://www.liveforfilms.com/2014/01/23/the-hunger-games-mockingjay-part-1-gets-a-poster/&h=266&w=973&tbnid=3Nfkdgcif-dBZM:&docid=1ZjdoczZLpHrcM&ei=p_uVVsrSEIr_ab36nqAN&tbm=isch&ved=0ahUKEwiKwrT5oKbKAhWKfxoKHT29B9Q4ZBAzCA4oCzAL

Movie information:
http://www.canalwalk.co.za/movies.htm

Heart of sea image:
http://film21terbaru.net/wp-content/uploads/2015/12/in_the_heart_of_the_sea.jpg

Alvin and the chip munks image:
https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQU7SlAyO1Lah723H7x-vdWtcT8cqh5bTEIlKOEDW5saxsZCCUF4Q

Mocking Jay Part 2 Image:
http://cdn-static.denofgeek.com/sites/denofgeek/files/styles/insert_main_wide_image/public/6/24//mockingjay_part_2.jpg?itok=3DQfcFne

Last witch Hunter Image:
http://screenrant.com/wp-content/uploads/The-Last-Witch-Hunter-Movie-Poster-Vin-Diesel-2.jpg

Google Map - generated by:
http://maps-generator.com/

All videos are linablke to their youtube page

------------------------------------------------------------------------------------------------------
-------------------------------------- END OF FILE ---------------------------------------------------
------------------------------------------------------------------------------------------------------